import { FC } from "react";
import { Meetup } from "../../../models/meetup";
import { Card } from "../../Card";
import classes from "./MeetupItem.module.css";

type Item = Omit<Meetup, "id" | "description">;

interface MeetupItemProps extends Item {
  isFavorite?: boolean;
  onToogleFavorite: () => void;
  onViewItem: () => void;
}

const MeetupItem: FC<MeetupItemProps> = (props) => {
  return (
    <Card>
      <li className={classes.item}>
        <div className={classes.image}>
          <img src={props.image} alt={props.title} />
        </div>

        <div className={classes.content}>
          <h3>{props.title}</h3>
          <address>{props.address}</address>
        </div>

        <div className={classes.actions}>
          <button onClick={props.onToogleFavorite}>
            {props?.isFavorite ? "Remove from Favorites" : "To Favorites"}
          </button>

          <button onClick={props.onViewItem}>{"View"}</button>
        </div>
      </li>
    </Card>
  );
};

export default MeetupItem;
