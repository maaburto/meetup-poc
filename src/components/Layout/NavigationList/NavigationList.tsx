import { FC } from "react";

import { NavRoutes } from "../layout.interface";
import { NavigationItem } from "../NavigationItem";

interface NavigationListProps {
  navRoutes: NavRoutes[];
}

const NavigationList: FC<NavigationListProps> = (props) => {
  return (
    <ul>
      {props.navRoutes.map((route) => (
        <NavigationItem
          key={route.linkTo}
          linkTo={route.linkTo}
          name={route.name}
          badgeDescription={route?.badgeDescription}
        />
      ))}
    </ul>
  );
};

export default NavigationList;
