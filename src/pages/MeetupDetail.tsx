import { FC } from "react";
import { ReactQueryDevtools } from "react-query/devtools";
import { MeetupDetailedWrapper } from "../features";

const MeetupDetailPage: FC = () => {
  return (
    <section>
      <MeetupDetailedWrapper />
      <ReactQueryDevtools />
    </section>
  );
};

export default MeetupDetailPage;
