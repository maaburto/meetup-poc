import { useSelector } from "react-redux/es/exports";
import { RootState } from "../../../store";
import { NavRoutes } from "../layout.interface";
import { NavigationList } from "../NavigationList";
import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  const totalFavorites = useSelector(
    (state: RootState) => state.favoriteMeetups.totalFavorites
  );

  const navRoutes: NavRoutes[] = [
    {
      name: "All Meetups",
      linkTo: "/",
    },
    {
      name: "Add New Meetup",
      linkTo: "/new-meetup",
    },
    {
      name: "My Favorites",
      linkTo: "/favorites",
      badgeDescription: totalFavorites.toString(),
    },
  ];

  return (
    <header className={classes["header"]}>
      <div className={classes["logo"]}>React Meetups</div>
      <nav>
        <NavigationList navRoutes={navRoutes} />
      </nav>
    </header>
  );
};

export default MainNavigation;
