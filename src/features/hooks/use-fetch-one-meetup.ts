import { useQuery } from "react-query";
import { secondsToMilliseconds } from "../../helpers";
import { MEETUP_DETAIL } from "../../helpers/query-client";
import { fetchMeetup } from "../services/meetups";

export const useFetchOneMeetup = (meetupId: string) => {
  const {
    data: meetup,
    error,
    isLoading,
    isFetching,
    isSuccess
  } = useQuery([MEETUP_DETAIL, meetupId], fetchMeetup, {
    staleTime: secondsToMilliseconds(3600),
  });

  return { error, isLoading, isFetching, meetup, isSuccess };
};
