import { FC } from "react";
import { Card } from "../Card";
import classes from "./MeetupDetail.module.css";
import MeetupDetailItem, { MeetupDetailItemProps } from "./MeetupDetailItem";

interface MeetupDetailProps extends MeetupDetailItemProps {
  loading: boolean;
}

const MeetupDetail: FC<MeetupDetailProps> = (props) => {
  if (props.loading) {
    return (
      <div className={classes.loading}>
        <h2>Loading...</h2>
      </div>
    );
  }

  return (
    <Card>
      <MeetupDetailItem {...props} />
    </Card>
  );
};

export default MeetupDetail;
