export type ChildrenReactNode = JSX.Element[] | JSX.Element | React.ReactNode;

export type SortingBasicCompareFn = (a: number | string, b: number | string, isAsc: boolean) => number;
