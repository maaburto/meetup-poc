import { FC } from "react";
import classes from "./Card.module.css";
import { ChildrenReactNode } from "../../models/general";

interface CardProps {
  children: ChildrenReactNode;
}

const Card: FC<CardProps> = (props) => {
  return <div className={classes.card}>{props.children}</div>;
};

export default Card;
