import { NewMeetUp } from "../../components/Meetup";
import { Meetup } from "../../models/meetup";
import {
  transformToMeetupList,
  transformToMeetupRecord,
} from "../helpers/meetup-trasnformation-data";
import { QueryFunctionContext } from 'react-query'

const MEETUP_REST_URL = `${import.meta.env.VITE_FIREBASE_URL}/meetup`;

interface FetchMeetupReturn {
  dataInArray: Meetup[];
  dataInRecord?: Record<string, Meetup>;
}
type FetchMeetupListFn = (
  generateRecord: boolean
) => Promise<FetchMeetupReturn>;

export const fetchMeetupList: FetchMeetupListFn = async (generateRecord) => {
  return fetch(`${MEETUP_REST_URL}.json`)
    .then((response) => response.json())
    .then((data) => {
      const result: FetchMeetupReturn = {
        dataInArray: transformToMeetupList(data),
      };

      if (generateRecord) {
        result.dataInRecord = transformToMeetupRecord(data);
      }

      return result;
    });
};

export const createMeetup = async (newMeetup: NewMeetUp) => {
  return fetch(`${MEETUP_REST_URL}.json`, {
    body: JSON.stringify(newMeetup),
    headers: {
      "Content-Type": "application/json",
    },
    method: "POST",
  });
};

export async function fetchMeetup({ queryKey }: QueryFunctionContext): Promise<Meetup> {
  const [, meetId] = queryKey
  return fetch(`${MEETUP_REST_URL}/${meetId}.json`, {
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => data);
}
