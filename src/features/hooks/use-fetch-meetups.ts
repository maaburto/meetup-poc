import { useQuery } from "react-query";
import { secondsToMilliseconds } from "../../helpers";
import { MEETUP_LIST } from "../../helpers/query-client";
import { fetchMeetupList } from "../services/meetups";

interface UseFetchMeetupsParams {
  generateRecord: boolean;
}

export const useFetchMeetups = (
  params: UseFetchMeetupsParams = { generateRecord: false }
) => {
  const { generateRecord } = params;
  const { data, error, isLoading, isFetching } = useQuery(
    MEETUP_LIST,
    () => fetchMeetupList(generateRecord),
    {
      staleTime: secondsToMilliseconds(3600),
    }
  );
  const { dataInArray: meetups, dataInRecord } = data || {
    dataInArray: [],
  };

  return { error, isLoading, isFetching, meetups, dataInRecord };
};
