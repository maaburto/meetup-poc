import { Meetup } from "../../../models/meetup";

export type NewMeetUp = Omit<Meetup, "id">;
